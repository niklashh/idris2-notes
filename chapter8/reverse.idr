import Data.Vect

myReverse : {len : _} -> Vect len a -> Vect len a
myReverse [] = []
myReverse {len = S len} (x :: xs) = let result = myReverse xs ++ [x] in
                                        rewrite plusCommutative 1 len in result