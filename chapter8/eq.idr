-- import Data.Vect
import Decidable.Equality
-- import Control.Function

data Vect : Nat -> Type -> Type where
     Nil : Vect Z a
     (::) : (x : a) -> (xs : Vect k a) -> Vect (S k) a


twoPlusTwoNotFive : 2 + 2 = 5 -> Void
twoPlusTwoNotFive Refl impossible

twoPlusTwoFour : 2 + 2 = 4 -> Type
twoPlusTwoFour Refl = 0 = 0

valueNotSuc : (x : Nat) -> x = S x -> Void
valueNotSuc 0 Refl impossible -- ???

sucNotZero : S x = 0 -> Void
sucNotZero Refl impossible

zeroNotSuc : 0 = S x -> Void
zeroNotSuc Refl impossible

noRec : (contra : (k = l) -> Void) -> (S k = S l) -> Void
noRec contra Refl = contra Refl

checkEqNat : (num1 : Nat) -> (num2 : Nat) -> Dec (num1 = num2)
checkEqNat Z Z = Yes $ Refl {x = Z}
checkEqNat (S k) Z = No sucNotZero
checkEqNat Z (S k) = No zeroNotSuc
checkEqNat (S k) (S l) = case checkEqNat k l of
	   Yes Refl => Yes $ Refl {x = S k}
	   No contra => No (noRec contra)

exactLength : {m : _} ->
	      (len : Nat) -> (input : Vect m a) -> Maybe (Vect len a)
exactLength {m} len input = case decEq m len of
	    Yes Refl => Just input
	    No contra => Nothing

headUnequal : DecEq a => {xs : Vect n a} -> {ys : Vect n a} ->
	    (contra : (x = y) -> Void) -> ((x :: xs) = (y :: ys)) -> Void
headUnequal contra Refl = contra Refl

tailUnequal : DecEq a => {xs : Vect n a} -> {ys : Vect n a} ->
	    (contra : (xs = ys) -> Void) -> ((x :: xs) = (y :: ys)) -> Void
tailUnequal contra Refl = contra Refl -- how's this not id? because the implicit arguments differ

DecEq a => DecEq (Vect n a) where
      decEq [] [] = Yes $ Refl {x = []}
      decEq (l :: ls) (r :: rs) = case (decEq l r, decEq ls rs) of
            (Yes Refl, Yes Refl) => Yes Refl
            (_, No contra) => No $ tailUnequal contra
            (No contra, _) => No $ headUnequal contra

	    
