data EqNat : (num1 : Nat) -> (num2 : Nat) -> Type where
  Same : (num : Nat) -> EqNat num num

total
eqNat56 : EqNat 5 6 -> Void
eqNat56 _ impossible

data Vect : Nat -> Type -> Type where
     Nil  : Vect Z a
     (::) : a -> Vect l a -> Vect (S l) a

sameS : {k : _} -> {j : _} -> (eq : EqNat k j) -> EqNat (S k) (S j)
sameS {k} (Same k) = Same (S k)

checkEqNat : (num1 : Nat) -> (num2 : Nat) -> Maybe (EqNat num1 num2)     
checkEqNat 0 0 = Just (Same 0)
checkEqNat 0 (S k) = Nothing
checkEqNat (S k) 0 = Nothing
checkEqNat (S k) (S j) = case checkEqNat k j of
  Just (Same k) => Just $ Same (S k)
  Nothing => Nothing

exactLength : {l : _} ->
          (len : Nat) -> (input : Vect l a) -> Maybe (Vect len a)
exactLength {l} len input = case checkEqNat l len of
                                 Nothing => Nothing
                                 Just (Same len) => Just input
