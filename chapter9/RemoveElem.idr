module RemoveElem

import Decidable.Equality
import Data.Vect
import Data.Vect.Elem

total
export
removeElem : DecEq a => {len : _} -> (value : a) -> (xs : Vect (S len) a) -> {auto prf : Elem value xs} -> Vect len a
removeElem value (value :: ys) {prf = Here} = ys
removeElem {len = Z} value (y :: []) {prf = There later} = absurd later
removeElem {len = S k} value (y :: ys) {prf = There later} = y :: removeElem value ys

