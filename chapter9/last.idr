import Decidable.Equality

%default total

data Last : List a -> a -> Type where
     LastOne  : {value : a} ->
                Last [value] value
     LastCons : {x : a} -> {xs : List a} ->
                (prf : Last xs value) -> Last (x :: xs) value

notLastEmpty : Last [] a -> Void
notLastEmpty _ impossible

isNotLast : (contra : Last xs value -> Void) ->
            (emptyContra : xs = [] -> Void) ->
            Last (x :: xs) value -> Void
isNotLast _ emptyContra LastOne = emptyContra Refl
isNotLast contra _ (LastCons lastPrf) = contra lastPrf

isNotEqLast : (contra : x = value -> Void) -> Last [x] value -> Void
isNotEqLast contra LastOne = contra Refl
-- isNotEqLast contra (LastCons _) impossible -- not needed

isLast : DecEq a => (xs : List a) -> (value : a) -> Dec (Last xs value)
isLast [] _ = No notLastEmpty
isLast (x :: xs) value = case decEq xs [] of
       Yes Refl => case decEq x value of
           Yes Refl => Yes $ LastOne
           No contra => No $ isNotEqLast contra
       No xsEmptyContra => case isLast xs value of
          Yes lastPrf => Yes $ LastCons lastPrf
          No lastContra => No $ isNotLast lastContra xsEmptyContra

