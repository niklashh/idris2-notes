import Data.Vect
import Data.Vect.Elem
import Data.String
import Decidable.Equality

import RemoveElem

data WordState : (remaining : Nat) -> (letters : Nat) -> Type where
     MkWordState : (word : String) ->
                   (missing : Vect letters Char) -> 
                   WordState remaining letters

data Finished : Type where
     Lost : (game : WordState 0 (S letters)) -> Finished
     Won  : (game : WordState (S remaining) 0) -> Finished

     
data ValidInput : List Char -> Type where
     Letter : (c : Char) -> ValidInput [c]
    
isNotValidNil : ValidInput [] -> Void
isNotValidNil (Letter _) impossible

isNotValidMultiple : ValidInput (x :: y :: cs) -> Void
isNotValidMultiple (Letter _) impossible
        
isValidInput : (cs : List Char) -> Dec (ValidInput cs)
isValidInput [] = No isNotValidNil
isValidInput [c] = Yes $ Letter c
isValidInput (x :: y :: cs) = No isNotValidMultiple

isValidString : (s : String) -> Dec (ValidInput (unpack s))
isValidString s = isValidInput $ unpack s

readGuess : IO (x ** ValidInput x)
readGuess = do putStr "Guess: "
               x <- getLine
               case isValidString (toUpper x) of
                    Yes prf => pure (_ ** prf)
                    No contra => do putStrLn "Invalid guess"
                                    readGuess
               
processGuess : {letters : _} ->
               (letter : Char) ->
               WordState (S remaining) (S letters) ->
               Either (WordState remaining (S letters))
                      (WordState (S remaining) letters)
processGuess letter (MkWordState word missing)
             = case isElem letter missing of
                    Yes prf => Right $ MkWordState word $ removeElem letter missing
                    No contra => Left $ MkWordState word missing
  
game : {remaining : _} -> {letters : _} ->
       WordState (S remaining) (S letters) -> IO Finished
game {remaining} {letters} state
     = do (_ ** Letter letter) <- readGuess
          case processGuess letter state of
               Left l => do putStrLn $ "Letter not in the word! "
                                       ++ show remaining
                                       ++ " guesses remaining"
                            case remaining of
                                 0 => pure (Lost l)
                                 S _ => game l
               Right r => do putStrLn "You guessed correctly"
                             case letters of
                                  0 => pure (Won r)
                                  S _ => game r

main : IO ()
main = do result <- game {remaining=2} (MkWordState "Test" ['T', 'E', 'S'])
          case result of
               Lost (MkWordState word missing) =>
                    putStrLn ("You lose. The word was " ++ word)
               Won game => putStrLn "You win!"
