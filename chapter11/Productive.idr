import Data.Stream

countFrom : Integer -> Stream Integer
countFrom x = x :: countFrom (x + 1)

getPrefix : (count : Nat) -> Stream ty -> List ty
getPrefix 0 _ = []
getPrefix (S n) (x :: xs) = x :: getPrefix n xs

range : Integer -> Nat -> List Integer
range start count = getPrefix count (countFrom start)

labelWith : Stream labelType -> List a -> List (labelType, a)
labelWith _ [] = []
labelWith (l :: ls) (x :: xs) = (l, x) :: labelWith ls xs

label : List a -> List (Integer, a)
label = labelWith (iterate (+1) 0)
