import Data.Stream

everyOther : Stream a -> Stream a
everyOther (drop :: take :: rest) = take :: everyOther rest

data InfList : Type -> Type where
     (::) : a -> Inf (InfList a) -> InfList a

countFrom : Integer -> InfList Integer
countFrom val = val :: countFrom (val + 1)

getPrefix : Nat -> InfList a -> List a
getPrefix Z _ = []
getPrefix (S k) (x :: xs) = x :: getPrefix k xs

Functor InfList where
        map f (x :: xs) = f x :: map f xs

data Face = Heads | Tails

getFace : Int -> Face
getFace n = if n `mod` 2 == 0 then Heads else Tails

randoms : Int -> Stream Int
randoms seed = let seed' = 1664525 * seed + 1013904223 in
                   (seed' `div` 4) :: randoms seed'

coinFlips : (count : Nat) -> Stream Int -> List Face
coinFlips count = take count . (map getFace)

squareRootApproxIter : Double -> Double -> Double
squareRootApproxIter number approx = (approx + (number / approx)) / 2

squareRootApprox : (number : Double) -> (approx : Double) -> Stream Double
squareRootApprox number approx = iterate (squareRootApproxIter number) approx

squareRootBound : (max : Nat)
               -> (number : Double)
               -> (bound : Double)
               -> (approxs : Stream Double)
               -> Double

