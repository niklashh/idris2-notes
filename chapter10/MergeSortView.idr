import Data.List.Views
import Data.List

mergeSort : Ord a => List a -> List a
mergeSort input with (splitRec input)
  mergeSort [] | _ = []
  mergeSort [x] | _ = [x]
  mergeSort (lefts ++ rights) | (SplitRecPair _ _ lrec rrec) =
    merge (mergeSort lefts | lrec) (mergeSort rights | rrec)
