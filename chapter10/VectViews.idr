import Data.Vect
import Data.Nat
import Control.WellFounded

data Split : Vect n a -> Type where
     SplitNil : Split []
     SplitOne : Split [x]
     SplitPair : {x, y : a} -> {xs : Vect n a} -> {ys : Vect m a} ->
                 Split (x :: xs ++ y :: ys)

splitHelp : (head : a) ->
            (xs : Vect n a) -> 
            (counter : Vect m a) -> Split (head :: xs)
splitHelp head [] counter = SplitOne
splitHelp head (x :: xs) [] = SplitPair {xs = []} {ys = xs}
splitHelp head (x :: xs) [y] = SplitPair {xs = []} {ys = xs}
splitHelp head (x :: xs) (_ :: _ :: ys) 
     = case splitHelp head xs ys of
            SplitOne => SplitPair {xs = []} {ys = []}
            SplitPair {xs = xs'} {ys = ys'}
              => SplitPair {xs = x :: xs'} {ys = ys'}

split : (xs : Vect n a) -> Split xs
split [] = SplitNil
split (x :: xs) = splitHelp x xs xs

data SplitRec : Vect n a -> Type where
     SplitRecNil : SplitRec []
     SplitRecOne : {x : a} -> SplitRec [x]
     SplitRecPair : {xs : Vect n a} ->
                    {ys : Vect n a} ->
                    (lrec : Lazy (SplitRec xs)) ->
                    (rrec : Lazy (SplitRec ys)) ->
                    SplitRec (xs ++ ys)


smallerPlusL : {m : _} -> {k : _} -> LTE (S (S m)) (S (plus m (S k)))
smallerPlusL {m} {k} = rewrite sym (plusSuccRightSucc m k) in
                              (LTESucc (LTESucc (lteAddRight _)))

smallerPlusR : {m : _} -> {k : _} -> LTE (S (S k)) (S (plus m (S k)))
smallerPlusR {m} {k} = rewrite sym (plusSuccRightSucc m k) in
                               LTESucc (LTESucc (rewrite plusCommutative m k in (lteAddRight _)))

                                                              
-- splitRec : {n : _} -> (input : Vect n a) -> SplitRec xs
-- splitRec input with (split input)
--     splitRec [] | SplitNil = ?wSplitRecNil
--     splitRec [x] | SplitOne = ?wSplitRecOne
--     splitRec (x :: (xs' ++ y :: ys')) | SplitPair {xs = xs'} {ys = ys'}
--       = SplitRecPair (splitRec (x :: xs'))
--                      (splitRec (y :: ys'))

splitRec : {n : Nat} -> (xs : Vect n a) -> SplitRec xs
splitRec {n, a} input with (sizeAccessible n)
  splitRec {n, a} input | acc with (split input)
    splitRec {n = 0, a} [] | acc | SplitNil = SplitRecNil
    splitRec {n = 1, a} [x] | acc | SplitOne = SplitRecOne
    splitRec {n = S l + S k, a} (x :: xs' ++ y :: ys') | Access acc | SplitPair {xs = xs'} {ys = ys'} {n = l} {m = k}
        = SplitRecPair
            (splitRec (x :: xs') | acc _ smallerPlusL)
            (splitRec (y :: ys') | acc _ smallerPlusR)
