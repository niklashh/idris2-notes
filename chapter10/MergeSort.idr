import Data.List

data SplitList : List a -> Type where
     SplitNil : SplitList []
     SplitOne : SplitList [x]
     SplitPair : (lefts : List a) -> (rights : List a) -> SplitList (lefts ++ rights)

splitList : (input : List a) -> SplitList input
splitList input = splitList' (length input) input
  where splitList' : Nat -> (input : List a) -> SplitList input
        splitList' _ [] = SplitNil
        splitList' _ [x] = SplitOne
        splitList' (S (S counter)) (item :: items) =
          case splitList' counter items of
               SplitNil => SplitOne
               SplitOne {x} => SplitPair [item] [x]
               SplitPair lefts rights => SplitPair (item :: lefts) rights
        splitList' _ items = SplitPair [] items

mergeSort : Ord a => List a -> List a
mergeSort input with (splitList input)
  mergeSort [] | SplitNil = []
  mergeSort [x] | SplitOne = [x]
  mergeSort (lefts ++ rights) | SplitPair lefts rights =
    merge (mergeSort lefts) (mergeSort rights)
