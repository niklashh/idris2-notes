import Data.List
import Data.Nat

%default total

data TakeN : List a -> Type where
     Fewer : TakeN xs
     Exact : (n_xs : List a) -> {rest : _} -> TakeN (n_xs ++ rest)

takeN : (n : Nat) -> (xs : List a) -> TakeN xs
takeN Z _ = Exact []
takeN (S k) [] = Fewer
takeN (S k) (x :: xs) =
  case takeN k xs of
       Fewer => Fewer
       Exact k_xs => Exact (x :: k_xs)

partial
sliding : (n : Nat) -> (xs : List a) -> List (List a)
sliding n xs with (takeN n xs)
  sliding n xs | Fewer = [xs]
  sliding n (n_xs ++ rest) | (Exact n_xs) = n_xs :: sliding n rest

natDiv2 : Nat -> Nat
natDiv2 (S (S k)) = S $ natDiv2 k
natDiv2 _ = Z

-- data TakeNRefined : List a -> Type where
--      Took : (n_xs : List a) -> {rest : _} -> TakeNRefined (n_xs ++ rest)

-- takeHalf : (xs : List a) -> TakeNRefined xs
-- takeHalf xs = takeHalf' xs xs
--   where takeHalf' : List a -> (xs : List a) -> TakeNRefined xs
--         takeHalf' [] xs = Took []
--         takeHalf' (_ :: _ :: rest) (x :: xs) =
--           case takeHalf' rest xs of
--                Took ys => Took (x :: ys) -- how to unify?
--         takeHalf' _ xs = Took xs

halves : List a -> (List a, List a)
halves xs with (takeN (natDiv2 $ length xs) xs)
  halves (lefts ++ rights) | Exact _ = (lefts, rights)
  halves _ | Fewer = ?rhs -- how to prove this is impossible
