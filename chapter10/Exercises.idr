import Data.List.Views
import Data.Vect
import Data.Nat.Views
import VectViews

%default total

equalSuffix : Eq a => List a -> List a -> List a
equalSuffix suf parent with (snocList suf)
  equalSuffix [] parent | _ = []
  equalSuffix (xs ++ [x]) parent | (Snoc _ _ xsrec) with (snocList parent)
    equalSuffix (xs ++ [x]) [] | (Snoc _ _ xsrec) | _ = []
    equalSuffix (xs ++ [x]) (ys ++ [y]) | (Snoc _ _ xsrec) | (Snoc _ _ ysrec) =
      -- there is no | ysrec here because i guess idris 2 can't find
      -- the second with clause, add it and get this: Error: With
      -- clause does not match parent.  This probably means the parent
      -- view is constructed on every call.  Also, try removing |
      -- xsrec and notice how incredibly slow it gets
      if x == y then (equalSuffix xs ys | xsrec) ++ [x]
                else []
