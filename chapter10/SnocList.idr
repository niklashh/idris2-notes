%default total

data SnocList : List a -> Type where
     Empty : SnocList []
     Snoc : {x, xs : _} -> (rec : SnocList xs) -> SnocList (xs ++ [x])


appendNilNeutral : (xs : List a) -> xs ++ [] = xs
appendNilNeutral [] = Refl
appendNilNeutral (x :: xs) = rewrite appendNilNeutral xs in Refl

appendAssociative : (l : List a) -> (c : List a) -> (r : List a) ->
                    l ++ (c ++ r) = (l ++ c) ++ r
appendAssociative [] c r = Refl
appendAssociative (l :: ls) c r = rewrite appendAssociative ls c r in Refl

snocList' : {input : _} ->
            SnocList input -> (rest : List a) -> SnocList (input ++ rest)
snocList' {input} snoc [] = rewrite appendNilNeutral input in snoc
snocList' {input} snoc (x :: xs) =
  rewrite appendAssociative input [x] xs in
          snocList' (Snoc snoc {x}) xs

snocList : (xs : List a) -> SnocList xs
snocList xs = snocList' Empty xs

rev : List a -> List a
rev input with (snocList input)
  rev [] | Empty = []
  rev (xs ++ [x]) | Snoc rec = x :: rev xs | rec
