import SnocList

-- this function is partial because idris doesn't know that the pair
-- (snocList suffix, snocList super) gets smaller on each recursion
isSuffix : Eq a => List a -> List a -> Bool
isSuffix suffix super with (snocList suffix, snocList super)
  isSuffix (xs ++ [x]) (ys ++ [y]) | (Snoc xsrec, Snoc ysrec)
    = (x == y) && (isSuffix xs ys | (xsrec, ysrec))
  isSuffix _ _ | (Empty, _) = True
  isSuffix _ _ | (_, Empty) = False
