data State = Open | Closed

data Result = OK | Jammed

data Cmd : (ty : Type) -> State -> (ty -> State) -> Type where
     Openit   : Cmd Result Closed (\res =>
                    case res of
                         OK => Open
                         Jammed => Closed)
     Close    : Cmd () Open   (const Closed)
     RingBell : Cmd () Closed (const Closed)
     Display  : String -> Cmd () state (const state)
     
     Pure     : ty -> Cmd ty (stateFn res) stateFn
     (>>=)    : Cmd a state1 state2Fn ->
                ((res : a) -> Cmd b (state2Fn res) state3Fn) ->
                (Cmd b state1 state3Fn)
                
doorProg : Cmd () Closed (const Closed)
doorProg = do _ <- RingBell
              OK <- Openit | Jammed => Display "Door jammed"
              _ <- Display "Door opened successfully"
              Close
