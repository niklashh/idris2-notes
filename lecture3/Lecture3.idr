%default total


countFrom : Nat -> Stream Nat
countFrom k = k :: countFrom (S k)


takes : Nat -> Stream a -> List a
takes Z _ = []
takes (S k) (x :: xs) = x :: takes k xs


data Command : Type -> Type where
     PutStr : String -> Command ()
     PutStrLn : String -> Command ()
     GetStr : Command String

data InfIO : Type where
     Do : Command a -> (a -> Inf InfIO) -> InfIO
     
(>>=) : Command a -> (a -> Inf InfIO) -> InfIO
(>>=) = Do

main : InfIO
main = do _ <- PutStr "Name? "
          name <- GetStr
          _ <- PutStrLn ("Hello " ++ name)
          main

runCommand : Command a -> IO a
runCommand (PutStr s) = putStr s
runCommand (PutStrLn s) = putStrLn s
runCommand GetStr = getLine

-- partial
-- run : InfIO -> IO ()
-- run (Do cmd cont) = do result <- runCommand cmd
--                        run (cont result)


data Fuel = Dry | More (Lazy Fuel)

tank : Nat -> Fuel
tank Z = Dry
tank (S f) = More (tank f)

runTotal : Fuel -> InfIO -> IO ()
runTotal Dry _ = putStrLn "You need more fuel to run the engine"
runTotal (More f) (Do cmd cont) = do result <- runCommand cmd
                                     runTotal f (cont result)

partial -- minimize the amount of complexity in partial functions
forever : Fuel
forever = More forever
